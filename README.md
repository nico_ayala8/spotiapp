# SPOTI-APP
```
Web-App con la API de Spotify
```

# Requisitos
```
NPM
NodeJS
Postman (https://www.postman.com)
```

# Caracteristicas
```
Muestra de los últimos 20 releases en Spotify en tarjetas
Información sobre el artista, datos, y los 10 tracks más relevantes. También con un preview de las canciones
Buscador integrado
```
# IMPORTANTE
```
Para utilizar la API de Spotify se requiere de un token personalizado:

1. Generar una APP nueva desde el sitio oficial de Spotify for Developers: https://developer.spotify.com/dashboard/applications
2. Se generan 2 keys con sus values: 'Client ID' y 'Client Secret'.
3. Desde Postman, setear de la siguiente manera y generar el token personalizado (expira cada una hora) y actualizar manualmente en el archivo "spotify.service"
```
![postman](https://i.ibb.co/mRQ8d4s/postman.png "postman")

# Capturas
![home](https://i.ibb.co/YfXT8pB/home.png "Home")
![artista](https://i.ibb.co/sKj2Zgc/artista.png "Artista")
![buscador](https://i.ibb.co/0cy5gTd/buscador.png "Search")

